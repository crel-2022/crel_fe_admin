# build stage
FROM node:14-alpine AS build-admin
WORKDIR /app
COPY package*.json ./
RUN npm i
COPY . .
RUN npm i -g @angular/cli
RUN ng build --base-href /admin/

# production stage
FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-admin /app/dist /usr/share/nginx/html